package com.lionseal.incursio;

import android.app.Dialog;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ObjectivoDialog extends Dialog implements android.view.View.OnClickListener {

	BoardScreen bs;

	public ObjectivoDialog(BoardScreen context) {
		super(context, R.style.MenuDialog);
		setContentView(R.layout.objetivo_dialog);
		setCanceledOnTouchOutside(false);
		setOnCancelListener(context);
		bs = context;
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/PWScratchy.ttf");
		TextView t = (TextView) findViewById(R.id.objetivo);
		t.setTypeface(tf);
		Button b = (Button) findViewById(R.id.button_10);
		b.setOnClickListener(this);
		b.setTypeface(tf);
		b = (Button) findViewById(R.id.button_20);
		b.setOnClickListener(this);
		b.setTypeface(tf);
		b = (Button) findViewById(R.id.button_endless);
		b.setOnClickListener(this);
		b.setTypeface(tf);
	}

	@Override
	public void onClick(View v) {
		int obj = Integer.MAX_VALUE;
		switch (v.getId()) {
		case R.id.button_10:
			obj = 10;
			break;
		case R.id.button_20:
			obj = 20;
			break;
		}
		bs.objetivoSelected(obj);
		dismiss();
	}
}
