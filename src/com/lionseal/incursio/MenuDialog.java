package com.lionseal.incursio;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

public class MenuDialog extends Dialog implements android.view.View.OnClickListener {
	public MenuDialog(Activity context, int score1, int score2, String next, boolean fin) {
		super(context, R.style.MenuDialog);
		setContentView(R.layout.dialog_menu);
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/PWScratchy.ttf");
		TextView t = (TextView) findViewById(R.id.score1);
		t.setTypeface(tf);
		t.setText(score1 + " pts");
		t = (TextView) findViewById(R.id.score2);
		t.setTypeface(tf);
		t.setText(score2 + " pts");
		t = (TextView) findViewById(R.id.next);
		t.setTypeface(tf);
		String objetivo = next + " " + t.getText().toString();
		if (fin) {
			if(score1 < score2){
				next = context.getResources().getString(R.string.player2);
			}else{
				next = context.getResources().getString(R.string.player1);
			}
			objetivo = context.getResources().getString(R.string.win) + " " + next;
		}
		t.setText(objetivo);

		t = (TextView) findViewById(R.id.player1);
		t.setTypeface(tf);
		t = (TextView) findViewById(R.id.player2);
		t.setTypeface(tf);
		t = (TextView) findViewById(R.id.tap);
		t.setTypeface(tf);

		findViewById(R.id.view_to_listen_for_touch).setOnClickListener(this);
	}

	public void onClick(View v) {
		dismiss();
	}
}
