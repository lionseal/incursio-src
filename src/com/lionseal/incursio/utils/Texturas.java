package com.lionseal.incursio.utils;

import java.util.HashMap;

import org.andengine.opengl.texture.region.ITiledTextureRegion;

import android.util.Log;

public class Texturas {
	private static final Texturas instance = new Texturas();

	public static final String CASILLA = "casilla";
	public static final String FICHA_P1 = "fichaP1";
	public static final String FICHA_P2 = "fichaP2";

	private HashMap<String, ITiledTextureRegion> tiledTextureBank = new HashMap<>();

	public static Texturas getInstance() {
		return instance;
	}

	public ITiledTextureRegion getTiledTexture(String key) {
		ITiledTextureRegion tex = tiledTextureBank.get(key);
		if (tex == null) {
			Log.e("Texture", "Texture missing: " + key);
		}
		return tex.deepCopy();
	}

	public void putTiledTexture(String key, ITiledTextureRegion t) {
		if (t != null)
			tiledTextureBank.put(key, t);
	}
}
