package com.lionseal.incursio.utils;

import java.util.HashMap;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;

public class Sonidos extends SoundPool {
	private static final Sonidos instance = new Sonidos();

	private HashMap<Integer, Integer> soundBank = new HashMap<>();
	private boolean shouldPlay;

	public static Sonidos getInstance() {
		return instance;
	}

	private Sonidos() {
		super(10, AudioManager.STREAM_MUSIC, 0);
	}

	public void load(final Activity context, final int resId) {
		if (soundBank.get(resId) == null)
			soundBank.put(resId, load(context, resId, 1));
	}

	public void play(final Activity context, int resId) {
		if (!shouldPlay)
			return;
		AudioManager audioManager = (AudioManager) context.getSystemService(Activity.AUDIO_SERVICE);
		float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = actualVolume / maxVolume;
		play(soundBank.get(resId), volume, volume, 1, 0, 1f);
	}

	public void setShouldPlay(boolean shouldPlay) {
		this.shouldPlay = shouldPlay;
	}
}
