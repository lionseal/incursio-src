/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lionseal.incursio.logic;

import com.lionseal.incursio.BoardScreen;

/**
 * 
 * @author Francisco
 */
public class Tablero {

	private final Casilla[][] aCasillas = new Casilla[9][5];
	private final Jugador[] jJugadores = new Jugador[2];
	private Boolean bTurno;
	private Movimiento mMovimiento;
	private final BoardScreen bsBoard;
	private Boolean bTurnoInicial;
	private String sLog;
	private boolean bFin;
	private int iObjetivo;

	public void WriteLog(String s) {
		sLog += s + "\n";
	}

	public String PrintLog() {
		return sLog;
	}

	public Casilla[][] getaCasillas() {
		return aCasillas;
	}

	public Jugador[] getjJugadores() {
		return jJugadores;
	}

	public Boolean isbTurno() {
		return bTurno;
	}

	public Movimiento getmMovimiento() {
		return mMovimiento;
	}

	public void setmMovimiento(Movimiento mMovimiento) {
		this.mMovimiento = mMovimiento;
	}

	public BoardScreen getBS() {
		return bsBoard;
	}

	public final Casilla getCasilla(int x, int y) {
		return aCasillas[x][y];
	}

	public Boolean getbTurnoInicial() {
		return bTurnoInicial;
	}

	public int getiObjetivo() {
		return iObjetivo;
	}

	public void setiObjetivo(int obj) {
		this.iObjetivo = obj;
	}

	public boolean isbFin() {
		return bFin;
	}

	public Tablero(BoardScreen bc) {
		this.sLog = "";
		this.bTurnoInicial = true;
		this.bsBoard = bc;
		this.bTurno = bTurnoInicial;
		this.bFin = false;
		Casilla c;
		for (int x = 0; x < 9; x++) {
			for (int y = 0; y < 5; y++) {
				c = new Casilla(x, y, this);
				aCasillas[x][y] = c;
			}
		}
		for (int x = 0; x < 9; x++) {
			for (int y = 0; y < 5; y++) {
				if (aCasillas[x][y].isBotTP() || aCasillas[x][y].isTopTP()) {
					int a, b;
					if (y == 0) {
						b = 4;
					} else {
						b = 0;
					}
					if (x == 2) {
						a = 6;
					} else {
						if (x == 6) {
							a = 2;
						} else {
							a = x;
						}
					}
					aCasillas[x][y].setcDestino(getCasilla(a, b));
				}
			}
		}
		Jugador j;
		j = new Jugador(true, this, 0);
		this.jJugadores[0] = j;
		j = new Jugador(false, this, 1);
		this.jJugadores[1] = j;
		mMovimiento = new Movimiento(this);
	}

	public void FinalizarPartida() {
		int iPuntosP1 = jJugadores[0].getiPuntos(), iPuntosP2 = jJugadores[1].getiPuntos();
		boolean bMuertasP1 = true, bMuertasP2 = true;
		for (Ficha f : jJugadores[0].getfFichas()) {
			if (f.geteEstado() != Ficha.Estado.MUERTA) {
				bMuertasP1 = false;
				if (f.enMeta()) {
					iPuntosP1++;
				}
			}
		}
		if (bMuertasP1) {
			iPuntosP2 += 5;
		}
		for (Ficha f : jJugadores[1].getfFichas()) {
			if (f.geteEstado() != Ficha.Estado.MUERTA) {
				bMuertasP2 = false;
				if (f.enMeta()) {
					iPuntosP2++;
				}
			}
		}
		if (bMuertasP2) {
			iPuntosP1 += 5;
		}
		jJugadores[0].setiPuntos(iPuntosP1);
		jJugadores[1].setiPuntos(iPuntosP2);
		if (iPuntosP1 >= iObjetivo || iPuntosP2 >= iObjetivo) {
			if (iPuntosP1 > iPuntosP2 + 1 || iPuntosP2 > iPuntosP1 + 1) {
				bFin = true;
			}
		}
		if (iPuntosP1 > iPuntosP2) {
			bTurnoInicial = jJugadores[1].isbTurno();
		} else {
			if (iPuntosP1 < iPuntosP2) {
				bTurnoInicial = jJugadores[0].isbTurno();
			} else {
				bTurnoInicial = !bTurnoInicial;
			}
		}
		bTurno = bTurnoInicial;
	}

	public void CambiarTurno() {
		this.bTurno = !bTurno;
		mMovimiento = new Movimiento(this);
		if (mMovimiento.isbFinPartida()) {
			FinalizarPartida();
			bsBoard.ReLoadBoard();
		}
	}

	public void NuevaPartida() {
		for (Jugador j : jJugadores) {
			j.BorrarFichas(this);
		}
		for (Jugador j : jJugadores) {
			j.CargarFichas(this);
		}
		mMovimiento = new Movimiento(this);
	}

}
