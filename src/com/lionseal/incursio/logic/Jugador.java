/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lionseal.incursio.logic;

/**
 * 
 * @author Francisco
 */
public class Jugador {

	// private final Tablero tTablero;
	// agregue para identificar las fichas mas facil
	private final int iJugador;
	private int iPuntos;
	private final Ficha[] fFichas = new Ficha[5];
	private final Boolean bTurno;

	public int getiJugador() {
		return iJugador;
	}

	public int getiPuntos() {
		return iPuntos;
	}

	public void setiPuntos(int iPuntos) {
		this.iPuntos = iPuntos;
	}

	public Ficha[] getfFichas() {
		return fFichas;
	}

	public Boolean isbTurno() {
		return bTurno;
	}

	public final void CargarFichas(Tablero t) {
		for (int i = 0; i < 5; i++) {
			Ficha f = new Ficha(i, bTurno, this, t);
			this.fFichas[i] = f;
		}
	}

	public Jugador(Boolean b, Tablero t, int j) {
		this.bTurno = b;
		// this.tTablero = t;
		this.iJugador = j;
		CargarFichas(t);
		this.iPuntos = 0;
	}

	public void BorrarFichas(Tablero t) {
		for (Ficha f : fFichas) {
			t.getCasilla(f.getiPosicion()[0], f.getiPosicion()[1]).setfOcupante(null);
		}
	}
}
